function Messenger3(resources)
{
	Messenger3.resources = resources;
	
}
Messenger3.prototype = {
	init: function()
	{

		this.game = new Phaser.Game(350, 700, Phaser.CANVAS, 'messenger_3', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this });
	},

	preload: function()
	{
		
		this.game.scale.maxWidth = 350;
		this.game.scale.maxHeight = 700;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.game.load.image('phone_shell', Messenger3.resources.phone_shell);
		this.game.load.image('head', Messenger3.resources.headicon);
		
		this.game.created = false;
		this.game.stage.backgroundColor = 'ffffff';

	},

	create: function(evt)
	{
		Phaser.Canvas.setTouchAction(this.game.canvas, "auto");
		this.game.input.touch.preventDefault = false;
		if(this.game.created === false)
		{
			//this.game.world.centerX-270
			this.parent.style = Messenger3.resources.textStyle_1;
			this.parent.message_zone = this.game.add.sprite();
			this.parent.message_zone.backgroundColor = 'ff0000';
			this.parent.phone_frame = this.game.add.sprite(0,0,'phone_shell');
			
			this.parent.message_zone.y = 410;
			this.game.messageAr = new Array();
			this.game.chatHeight = 0;
			this.game.created  = true;
			var timAR = new Array();
			
			var timer = this.game.time.create(false);
			for(var i = 0;i<Messenger3.resources.messages.length;i++)
			{
				var delay = Messenger3.resources.messages[i].delay;
				
				
				timer.add(1000*delay, this.parent.bubble_draw, this.parent,{message:Messenger3.resources.messages[i].message,direction:Messenger3.resources.messages[i].direction});
				timer.start();
			}
			


			this.parent.bubbleCount = 0;
			
		}
	},

	
	bubble_draw: function(config)
	{

		
		var padding = 4;
		var radius = 15;
		var bubble = this.game.add.sprite();
		var bubbleText = this.game.add.text(0,0,config.message,this.style);
		
		
		bubble.addChild(this.shapeBubble(bubbleText.width+(radius*2),bubbleText.height+(radius),config.direction,radius,bubble));
		bubble.addChild(bubbleText);
		var bubbleHeight = Number(bubbleText.height+radius)+padding;
		
		
		
		bubble.y = this.game.chatHeight;
		this.game.chatHeight+=bubbleHeight;
		bubbleText.smoothed = true;
		bubble.smoothed = true;
		bubbleText.x = (radius*2)+5;
		bubbleText.y = radius+10;
		bubble.x = 40;
		if(config.direction == "left")
		{
			bubbleText.fill = "#181818";
			var head = this.game.add.sprite(0,bubbleText.height,'head');
			bubble.addChild(head);
		}
		else
		{
			
			bubble.x = 270-bubbleText.width;
			bubbleText.fill = "#f8f8f8";
		}
		
		this.message_zone.addChild(bubble);
		
		
		
		
		var tw = this.game.add.tween(this.message_zone).to({y:this.message_zone.y-(bubbleHeight-radius)},350,Phaser.Easing.Quadratic.Out);

		tw.start();
		
		this.bubbleCount++;
		
		this.message_zone.smoothed = true;
		bubbleText.x = Math.round(bubbleText.x);
		

	},
	shapeBubble: function(width,height,direction,radius,parent)
	{
		var corner = radius*2;
		var bubbleColor = 0xf1f1f1;
		var bubbleStroke = 0xffffff;
		var leftBottomCorner = corner;
		var rightBottomCorner = width 
		
		switch(direction)
		{
			case "left":
				
				leftBottomCorner = corner+radius;
			break
			case "right":
				bubbleColor = 0x0284fe;
				 rightBottomCorner = width+radius
				
			break
			case "bottomLeft":
			break
			case "bottomRight":
			break

		}
		
		var graphics = this.game.add.graphics(0,0);
		graphics.lineStyle(2, bubbleStroke,2);
		graphics.beginFill(bubbleColor);
		graphics.moveTo(corner,corner);
		graphics.quadraticCurveTo(corner, radius, corner+radius,radius);
		graphics.lineTo(width,radius);
		graphics.quadraticCurveTo(width+radius, radius, width+radius,corner);
		graphics.lineTo(width+radius,height);
		graphics.quadraticCurveTo(width+radius, height+radius, rightBottomCorner,height+radius);
		graphics.lineTo(corner+radius,height+radius);
		graphics.quadraticCurveTo(corner,height+radius,corner,height);
		graphics.lineTo(corner,corner);
		
		return graphics

	},

	inview: function()
	{
		
		
	},

	animate: function()
	{
		//console.log("animate")
	},

	update: function()
	{

	},
	render: function()
	{
		//this.game.debug.inputInfo(32, 32);
	}

}





